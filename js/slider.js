// Кнопки next (регистрация / далее)
let btnNextOne = $(".next-one"),
    btnNextTwo = $(".next-two"),
    submitRegister = $(".register-submit");

// кнопки back (назад)
let btnBackOne = $('.back-one'),
    btnBackTwo = $('.back-two');

// иконка и заголовок
let icon = $(".icon"),
    iconText = $('.icon-text');

// формы регистрации
// let forms = $(".forms"),
//     secondRegisterForm = forms[0],
//     thirdRegisterForm = forms[1],
//     fourRegisterForm = forms[2];
let firstForm = $("#firstForm"),
    secondForm = $("#secondForm"),
    thirdForm = $("#thirdForm");

// тело (шапка, форма, дно)
let fullBody = $('.body-main');

// текущие размеры окна
let heightWindow,
    widthWindow;

// текущий сдвиг (сдвиг нужен из-за особенности слайдера)
let sizeOfShift;

// сброс для сдвига (для возвращения к текущему состоянию)
const resetShift = 0;

// переменная для px
let px = 'px';

// брейкпоинты для разных экранов
let mg = 992,
    lg = 768;

// названия брейпоинтов
let small = 'small',
    medium = 'medium',
    big = 'big';


// инициализируем стартовые параметры окна (считай конструктор!)
initialWindow();

// начальная инициализация окна
function initialWindow() {
    // делаем body равным размеру окна
    heightWindow = window.innerHeight;
    widthWindow = window.innerWidth;
    fullBody.css('height', heightWindow + px);
    fullBody.css('width', widthWindow + px);
    // определяем сдвиг для стартового размера окна
    defineShift();
    // определяем параметры логотипа для стартового размера окна
    defineIconParameters();
}

// определяем к какому брейкпоинту относится текущий размер окна
function defineBreakPoint() {
    if (widthWindow <= lg) {
        return small;
    } else 
    if (widthWindow > lg && widthWindow <= mg) {
        return medium;
    } else {
        return big;
    }
}

// определяем размеры сдвига, исходя из размера окна
function defineShift() {
    let breakpoint = defineBreakPoint();

    switch(breakpoint) {
        case small: 
            sizeOfShift = -45;
            break;
        case medium:
            sizeOfShift = -60;
            break;
        case big:
            sizeOfShift = -95;
            break;
        default: break;
    }
}

// функция для сдвига нужному блоку (с помощью добавления margin-left)
function addLeftMargin(form, sizeOfShift) {
    form.css('margin-left', sizeOfShift + px);
}

function resetLeftMargin(form) {
    form.css('margin-left', 0);
}

// изменение параметров логотипа при достижении определенного брейкпоинта
function defineIconParameters() {
    let breakpoint = defineBreakPoint();

    switch(breakpoint) {
        case big: 
            icon.css('margin-right', 10 + px);
            icon.css("padding", 0);
            icon.css('height', 50 + px);
            icon.css('width', 50 + px);
            iconText.css('margin-left', 0);
            break;
        default: 
            icon.css('margin-right', 0);
            icon.css('padding', 20 + px);
            icon.css('height', 40 + px);
            icon.css('width', 40 + px);
            iconText.css('margin-left', 10 + px);
            break; 
    }
}

function changeVisabilityForms(visibleForm, invisibleForm) {
    // change size of visible block
    visibleForm.removeClass('col-8');
    visibleForm.addClass('col-1');

    // make visible block invisible
    visibleForm.removeClass('visible');
    visibleForm.addClass('invisible');

    //change size of invisible block
    invisibleForm.removeClass('col-1');
    invisibleForm.addClass('col-8');

    // make invisible block vilisble
    invisibleForm.removeClass('invisible');
    invisibleForm.addClass('visible');
}

btnNextOne.on("click", function(event) {
    defineShift();
    changeVisabilityForms(firstForm, secondForm);
    addLeftMargin(firstForm, sizeOfShift);
    $('#login').keyup(function(event) {

        if (event.keyCode === 13) {
            $('#login').blur();
            btnNextTwo.click();
    }
});
    $('#password').keyup(function(event) {

        if (event.keyCode === 13) {
            $('#password').blur();
            btnNextTwo.click();
    }
});

    $('#email').keyup(function(event) {

        if (event.keyCode === 13) {
            $('#email').blur();
            btnNextTwo.click();
    }
});

    $('#repeatPassword').keyup(function(event) {

    if (event.keyCode === 13) {
        $('#repeatPassword').blur();
        btnNextTwo.click();
}
});

});


btnBackOne.on("click", function(event) {
    defineShift();
    changeVisabilityForms(secondForm, firstForm);
    resetLeftMargin(firstForm);
    $('#login').popover('hide');
    $('#password').popover('hide');
    $('#email').popover('hide');
    $('#repeatPassword').popover('hide');
});

btnNextTwo.click(function() {
    $('#login').popover('hide');
    $('#email').popover('hide');
    $('#password').popover('hide');
    $('#repeatPassword').popover('hide');
    $('#login').removeClass('bad-input');
    $('#email').removeClass('bad-input');
    $('#password').removeClass('bad-input');
    $('#repeatPassword').removeClass('bad-input');

    if(validate()) {
    defineShift();
    changeVisabilityForms(secondForm, thirdForm);
    addLeftMargin(thirdForm,sizeOfShift);
    }

    // defineShift();
    // changeVisabilityForms(thirdRegisterForm, fourRegisterForm);
    // addLeftMargin(fourRegisterForm,sizeOfShift);

});

// $('#login').on("focus", function() {
//     $('#login').popover('hide');
// });

btnBackTwo.on("click", function(event) {
    defineShift();
    changeVisabilityForms(thirdForm, secondForm);
    resetLeftMargin(thirdForm);
});


submitRegister.on("click", function() {
    let user = {
        "login": $("#login").val(),
        "pass": $("#password").val(),
        "email": $("#email").val()
    };
    $.post(
        "http://188.127.231.17:8004/reg",
        JSON.stringify(user),
        checkPost
      );

      $('#canvas').addClass('fade-body');
      $('.body-main').addClass('fade-body');
      thirdForm.removeClass('visible');
      thirdForm.addClass('invisible');
      $('.submit-registration').removeClass('invisible');
      $('.submit-registration').addClass('visible');

    $('.submit-email').html($("#email").val());
    $('#submit-login').html($("#login").val());
    $('#submit-password').html($("#password").val());
    $('#submit-firstName').html($("#firstName").val());

});

function checkPost(data) {
    console.log(data);
}


$(window).on("resize", function(event) {
    
    initialWindow();

});


// ВАЛИДАЦИЯ



function checkLogin() {
    let patternLogin = /^[a-z]+\w{3,32}/;
    let login = $("#login").val();
    if(!patternLogin.test(login)) {
        $('#login').popover('enable');
        $('#login').popover('show');
        $('#login').addClass('bad-input');
        $('#login').focus(function() {
            $('#login').removeClass('bad-input');
            $("#login").popover('hide');
        });
        return false;
    }
    return true;
}



function chechEmail() {
    let patternEmail = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    let email = $("#email").val();
    if(!patternEmail.test(email)) {
        $('#email').popover('enable');
        $('#email').popover('show');
        $('#email').addClass('bad-input');
        $('#email').focus(function() {
            $('#email').removeClass('bad-input');
            $("#email").popover('hide');
        });
        return false;
    }
    return true;
}

function checkPassword() {
    let patternPassword = /\w{6,32}/;
    let password = $("#password").val();
    if(!patternPassword.test(password)) {
        $('#password').popover('enable');
        $('#password').popover('show');
        $('#password').addClass('bad-input');
        $('#password').focus(function() {
            $('#password').removeClass('bad-input');
            $("#password").popover('hide');
        });
        return false;
    }
    return true;
}

function chechRepeatPassword() {

    let repeatPassword = $('#repeatPassword').val();
    if( !(repeatPassword === $('#password').val()) ) {
        $('#repeatPassword').popover('enable');
        $('#repeatPassword').popover('show');
        $('#repeatPassword').addClass('bad-input');
        $('#repeatPassword').focus(function() {
            $('#repeatPassword').removeClass('bad-input');
            $("#repeatPassword").popover('hide');
        });
        return false;
    }
    return true;
}

function validate() {

    if(checkLogin()) {

        $('#login').unbind('focus');
        $('#login').popover('disable');

        if(chechEmail()) {
            $('#email').unbind('focus');
            $('#email').popover('disable');

            if(checkPassword()) {
                $('#password').unbind('focus');
                $('#password').popover('disable');

                if(chechRepeatPassword()) {
                    $('#repeatPassword').unbind('focus');
                    $('#repeatPassword').popover('disable');
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }
    return false;
    // let email = $("#email").val();
    // let password = $("#password").val();
    // let repeatPassword = $("#repeatPassword").val();

    // console.log(login + ' ' + email + ' ' + password + ' ' + repeatPassword);
    // return false;
}

